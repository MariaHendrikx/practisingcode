
import java.util.Observable;
import java.util.Observer;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author maria
 */
public class calc_model extends Observable {
    String to_calc = "";
    String result = "";
    
    public void addToCalc(String str){
        to_calc += str;
        setChanged();
        notifyObservers(to_calc);
    }
    
    public void reset(){
        to_calc = ""; 
        result = "";
        setChanged();
        notifyObservers("");
    }
    
    public void calcResult(){
        try {
            int first_number = 0;
            int last_number = 0;
            String operation = "";

            Character x = to_calc.charAt(0);
            Character.isDigit(x);

            int i = 1;
            while (i < to_calc.length() && Character.isDigit(to_calc.charAt(i-1))) {
                first_number = Integer.parseInt(to_calc.substring(0, i));
                operation = to_calc.substring(i, i+1);
                last_number = Integer.parseInt(to_calc.substring(i+1));

                ++i;
            }
            
            System.out.print(Integer.toString(first_number) + operation + Integer.toString(last_number));
            
            result =  String.valueOf(calcResultHelper(first_number, last_number, operation));
            to_calc = "";
            setChanged();
            notifyObservers(result);
        } catch(Exception e){
            System.out.print("wrong input of user");
            throw e;
        }
    }
    
    
    
    private float calcResultHelper(int x1, int x2, String oper){
        try{
            switch(oper){
                case "+":
                    return x1 + x2;
                case "-":
                    return x1 - x2;
                case "*":
                    return x1 * x2;
                case "/":
                    return x1/x2;
                default:
                    return 0;        
            }
        } catch(Exception e){
            throw e;
        }
    }
    
    public String getResult(){return result;}
}
