
import java.util.ArrayList;
import java.util.Observable;
import javax.swing.GroupLayout;
import javax.swing.JButton;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author maria
 */
public class calc_controller {
    Observable c_model;
    calc_view c_view;
    
    public calc_controller(Observable model, calc_view view){
        c_model = model;
        c_view = view;
    }
    
    public void OnBtnClick(String str){
        System.out.print("in controller on btnclick \n ");
        ((calc_model)getModel()).addToCalc(str);
    }
    
    public void OnActionClick(String str){
        switch(str){
            case "Calc":
                ((calc_model)getModel()).calcResult();
                break;
            case "Del":
                ((calc_model)getModel()).reset();
                break;
            default:
                break;
        }
    }
    
    private calc_model getModel(){ return (calc_model) c_model;}
}
