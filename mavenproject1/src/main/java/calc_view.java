/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

/**
 *
 * @author maria
 */
public class calc_view implements Observer {
     private JFrame frame;
     private Observable c_model;
     private calc_controller c_controller;
     
     private JLabel text;
     private ArrayList<JButton> keys = new ArrayList<JButton>();
     private JButton plus;
     private JButton min;
     
     private GridBagLayout gridbag = new GridBagLayout();
    
    public calc_view(Observable model ,String title){
        c_model = model;
        c_controller = new calc_controller(c_model, this);
        
        initFrame(title);
        
        initText();
        initSymbols();
        initNumbers();
        initEnter();
        
        frame.pack();
    }
    
    private void initEnter(){
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 1.0;
        
        plus = makeActionBtn("Calc", gridbag, c);
        min = makeActionBtn("Del", gridbag, c);
    }
    
  
    private void initFrame(String title) {
        frame = new JFrame(title);
        frame.getContentPane().setLayout(new BorderLayout());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500, 300);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.setLayout(gridbag);
    }
    
    private JButton makebutton(String name,
                               GridBagLayout gridbag,
                               GridBagConstraints c) {
        JButton button = new JButton(name);
        gridbag.setConstraints(button, c);
        frame.add(button);
        
        // handle events: pass to controller
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                c_controller.OnBtnClick(e.getActionCommand());
            }
        });
            
        return button;
     }
    
    private JButton makeActionBtn(String name,
                               GridBagLayout gridbag,
                               GridBagConstraints c) {
        JButton button = new JButton(name);
        gridbag.setConstraints(button, c);
        frame.add(button);
        
        // handle events: pass to controller
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                c_controller.OnActionClick(e.getActionCommand());
            }
        });
            
        return button;
     }
    
    private void initText(){
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 1.0;
        c.gridwidth = GridBagConstraints.REMAINDER; 
        
        text = new JLabel("Olaf");
        gridbag.setConstraints(text, c);
        frame.add(text);
    }
    private void initNumbers() {
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 1.0;
        
        for (int i = 0; i < 10; ++i) {
            if( (i % 3) == 0){
                c.gridwidth = GridBagConstraints.REMAINDER; //end row
            } else{
                c.gridwidth = 1;
            }
            keys.add(makebutton(Integer.toString(i), gridbag, c));
        }
    }
    private void initSymbols() {
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 1.0;
        
        plus = makebutton("+", gridbag, c);
        min = makebutton("-", gridbag, c);
    }

    @Override
    public void update(Observable o, Object obj) {
        System.out.print("observer updated");
        text.setText((String) obj);
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
